//
//  RemoteData.swift
//  Em-HR Mobile Attendance
//
//  Created by aamaulana10 on 11/05/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation

struct RemoteData : Decodable {
    let created_at : String?
    let end_date : String?
    let id : Int?
    let latitude : Float?
    let location_name : String?
    let longitude : Float?
    let radius : Int?
    let start_date : String?
    let timezone : String?
    let updated_at : String?
    let user_id : Int?
}
