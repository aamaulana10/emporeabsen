//
//  ShiftDetail.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation

struct ShiftDetail1 : Decodable {
    let branch_id : Int?
    let created_at : String?
    let detail : [ShiftDataDetail]?
    let id : Int?
    let is_holiday : Int?
    let name : String?
    let updated_at : String?
    let workdays : String?
}

struct ShiftDataDetail : Decodable {
    let clock_in : String?
    let clock_out : String?
    let created_at : String?
    let day : String?
    let id : Int?
    let shift_id : Int?
    let updated_at : String?
}
