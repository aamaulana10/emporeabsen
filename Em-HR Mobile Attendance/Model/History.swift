//
//  History.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 27/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation

struct History : Decodable {
    let absensiDeviceId : Int?
    let absensi_id : Int?
    let absensi_setting_id : String?
    let absent : String?
    let ac_no : String?
    let att_time : String?
    let attendance_type_in : String?
    let attendance_type_out : String?
    let auto_assign : String?
    let clock_in : String?
    let clock_out : String?
    let created_at : String?
    let date : String?
    let department : String?
    let early : String?
    let emp_no : String?
    let exception : String?
    let holiday : String?
    let holiday_ot : String?
    let id : Int?
    let is_holiday : Int?
    let justification_in : String?
    let justification_out : String?
    let lat : String?
    let lat_office_in : Double?
    let lat_office_out : Double?
    let lat_out : String?
    let late : String?
    let long_field : String?
    let long_office_in : Double?
    let long_office_out : Double?
    let long_out : String?
    let must_c_in : String?
    let must_c_out : String?
    let name : String?
    let ndays : String?
    let ndays_ot : String?
    let no : String?
    let normal : String?
    let off_dutty : String?
    let on_dutty : String?
    let ot_time : String?
    let pic : String?
    let pic_out : String?
    let radius_office_in : Double?
    let radius_office_out : Double?
    let real_time : String?
    let shift_id : Int?
    let timetable : String?
    let timezone : String?
    let updated_at : String?
    let user_id : Int?
    let weekend : String?
    let weekend_ot : String?
    let work_time : String?
}

