//
//  Profile.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 28/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation

struct Profile : Decodable {
    let user : User?
    let company : [Company]?
    
}

struct Company : Decodable {
    
    let id : Int?
    let key : String?
    let value : String?
    let createdAt : String?
    let updatedAt : String?
    let descriptionField : String?
    let userCreated : Int?
    let projectId : Int?
    
}

struct User : Decodable {
    
    let id : Int?
    let nik : String?
    let tempat_lahir : String?
    let tanggal_lahir : String?
    let jenis_kelamin : String?
    let telepon : String?
    let foto : String?
    let foto_ktp : String?
    let email : String?
    let password : String?
    let name : String?
    let remember_token : String?
    let created_at : String?
    let updated_at : String?
    let agama : String?
    let last_logged_in_at : String?
    let last_logged_in_mobile : String?
    let last_logged_out_at : String?
    let access_id : Int?
    let status : Int?
    let division_id : Int?
    let ext : String?
    let ktp_number : String?
    let passport_number : String?
    let kk_number : String?
    let npwp_number : String?
    let jamsostek_number : String?
    let bpjs_number : String?
    let marital_status : String?
    let provinsi_id : Int?
    let kabupaten_id : Int?
    let kecamatan_id : Int?
    let kelurahan_id : Int?
    let id_zip_code : Int?
    let hak_cuti : Int?
    let cuti_yang_terpakai : Int?
    let sisa_cuti : Int?
    let cabang_id : Int?
    let nama_rekening : String?
    let nomor_rekening : String?
    let bank_id : Int?
    let join_date : String?
    let resign_date : String?
    let inactive_date : String?
    let current_address : String?
    let provinsi_current : Int?
    let kabupaten_current : Int?
    let kecamatan_current : Int?
    let kelurahan_current : Int?
    let current_zip_code : Int?
    let mobile_2 : String?
    let blood_type : String?
    let id_address : String?
    let mobile_1 : String?
    let length_of_service : String?
    let cuti_status : String?
    let branch_type : String?
    let organisasi_status : String?
    let statusContract : String?
    let start_date_contract : String?
    let end_date_contract : String?
    let cuti_2018 : String?
    let employee_number : Int?
    let absensi_number : String?
    let is_pic_cabang : Int?
    let is_generate_kontrak : Int?
    let generate_kontrak_file : String?
    let end_date : String?
    let no_kontrak : String?
    let empore_organisasi_direktur : String?
    let empore_organisasi_manager_id : Int?
    let empore_organisasi_staff_id : Int?
    let empore_organisasi_level : Int?
    let is_reset_first_password : Int?
    let last_change_password : String?
    let structure_organization_custom_id : Int?
    let project_id : Int?
    let apikey : String?
    let absensi_setting_id : Int?
    let status_active : Int?
    let shift_id : Int?
    let division : String?
    let position : String?
    let branch : Branch?
    let shift : Shift?
    
}

struct Branch : Decodable {
    
    let id : Int?
    let name : String?
    let description_field : String?
    let created_at : String?
    let updated_at : String?
    let alamat : String?
    let telepon : String?
    let fax : String?
    let latitude : Float?
    let longitude : Float?
    let radius : Int?
    let user_created : Int?
    let timezone : String?
    
}

struct Shift : Decodable {
    let branch_id : Int?
    let workdays : String?
    let name : String?
    let updated_at : String?
    let created_at : String?
    let id: Int?
    let is_holiday : Int?
}
