//
//  News.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 01/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation

struct News : Decodable {
    
    let author : Author?
    let content : String?
    let created_at : String?
    let id : Int?
    let image : String?
    let status : Int?
    let thumbnail : String?
    let title : String?
    let updated_at : String?
    let user_created : Int?
    
}

struct Author : Decodable {
    
    let id : Int?
    let name : String?
    let nik : String?
    
}
