//
//  BerandaController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BerandaController: UIViewController,  UINavigationControllerDelegate, UIImagePickerControllerDelegate, fromAbsenDelegate {
    
    @IBOutlet weak var waktuLabel: UILabel!
    @IBOutlet weak var clockStatus: UILabel!
    @IBOutlet weak var serverTime: UILabel!
    @IBOutlet weak var dateServer: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var btnClockIn: UIButton!
    @IBOutlet weak var img_Logo: UIImageView!
    @IBOutlet weak var inputContainer: UIView!
    @IBOutlet weak var quotes: UILabel!
    @IBOutlet weak var clockIn: UILabel!
    @IBOutlet weak var clockOut: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var stck1: UIStackView!
    @IBOutlet weak var marginTopLine: NSLayoutConstraint!
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    
    
    var activityView: UIActivityIndicatorView?
    
    var screenSize = UIScreen.main.bounds
    
    var code = ""
    var token = ""
    
    var dataProfile : Profile?
    var statusAbsen = 0
    var outOfOffice = ""
    var timer = Timer()
    var timer2 = Timer()
    
    var imagePicker: UIImagePickerController!
    var dataPhotoAbsen : Any = ""
    var time           : String?
    var timeStamp      = 0
    var timeStamp2      = 0
    
    var progressView    = ProgressView()
    var remote_attendace : RemoteData?
    var isRemote        = false
    var clockIndate     = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        validateToken()
        configView()
        getDataProfile()
        
        timer = Timer.scheduledTimer(timeInterval: 1.0,
                                     target: self,
                                     selector: #selector(tick),
                                     userInfo: nil,
                                     repeats: true)
        
        //        self.timer.invalidate()
        timer2 = Timer.scheduledTimer(timeInterval: 1.0,
                                      target: self,
                                      selector: #selector(tickDuration),
                                      userInfo: nil,
                                      repeats: true)
    }
    
    func fromAbsen(data: Bool) {
        if(data == true){
            validateToken()
            getDataProfile()
            getClockStatus()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        validateToken()
        configView()
        getDataProfile()
        getClockStatus()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        validateToken()
        configView()
        getDataProfile()
        setImageTitle()
        getClockStatus()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @objc func tick() {
        
        timeStamp += 1
        
        let customString = EmporeHelper.convertStringToDateFormater(time ?? "2010-10-10 10:20:30")
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "HH:mm:ss"
        
        let time1 = formatter.string(from: Date(timeInterval: TimeInterval(timeStamp), since: customString))
        
        print(time1)
        
        serverTime.text = time1
        
    }
    
    @objc func tickDuration() {
        
        timeStamp2 += 1
        
        print("clock in date ", clockIndate)
        
        if(clockIndate != ""){
            
        
        let clockInFormat = EmporeHelper.convertTimeFormater(clockIndate)
        //
        let formatter = DateFormatter()
        
        formatter.dateFormat = "HH:mm:ss"
        
        let time1 = clockInFormat
        let time2 = formatter.string(from: Date())
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "HH:mm"
        
        let date1 = formatter.date(from: time1)!
        let date2 = formatter.date(from: time2)!
        
        let elapsedTime = date2.timeIntervalSince(date1)
        
        // convert from seconds to hours, rounding down to the nearest hour
        let hours = floor(elapsedTime / 60 / 60)
        
        // we have to subtract the number of seconds in hours from minutes to get
        // the remaining minutes, rounding down to the nearest minute (in case you
        // want to get seconds down the road)
        let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
        
        print("\(Int(hours)) hr and \(Int(minutes)) min")
        
        print("elapsed time ",elapsedTime)
        
                
        duration.text = String(format: "%02d:%02d", Int(hours),Int(minutes))
        }
        else {
            duration.text = "----"
        }
        
    }
    
    
    func getCode(){
        if let companyCode = UserDefaults.standard.object(forKey: "code") as? String {
            code = companyCode
            
            print(code)
        }
        
    }
    
    func getToken(){
        if let dataToken = UserDefaults.standard.object(forKey: "token") as? String {
            token = dataToken
            
            print(token)
        }
        
    }
    
    func getClockStatus(){
        
        progressView.show(self, "Updating Data ...")
        
        getCode()
        getToken()
        
        let base_Url = Utils.base_url+"/clockstatus?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        Alamofire.request(base_Url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error)
                    self.progressView.hide()
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.getClockStatus()
                            
                        }
                    }
                }else {
                    self.progressView.hide()
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            let responseStatus : JSON = JSON(response.result.value!)
                            let status = responseStatus["status"]
                            let data = responseStatus["data"]
                            
                            print("data clock status ", responseStatus)
                            
                            if(status == "error"){
                                print("error ", response.result.value!)
                                let title = "Unstable Connection"
                                let message = "Please check your Connection & try again..."
                                self.showAlert(title: title, message: message)
                                
                            }else {
                                
                                if(data["out_of_office"].stringValue == "1") {
                                    print(data["out_of_office"].stringValue)
                                    self.outOfOffice = "1"
                                }else {
                                    print(data["out_of_office"].stringValue)
                                    self.outOfOffice = "0"
                                }
                                
                                
                                
                                if(data["type"].string == "branch"){
                                    
                                    self.time = data["branch_time"].string ?? ""
                                    self.waktuLabel.text = "\(data["type"].string?.capitalized ?? "") Timezone \(data["timezone"])"
                                    
                                    self.time = data["branch_time"].stringValue
                                    
                                }else if (data["type"].string == "server"){
                                    
                                    self.time = data["server_time"].string ?? ""
                                    self.waktuLabel.text = "\(data["type"].string?.capitalized ?? "") Timezone \(data["timezone"])"
                                    
                                    self.time = data["server_time"].stringValue
                                }else {
                                    
                                    self.time = data["remote_time"].string ?? ""
                                    self.waktuLabel.text = "\(data["type"].string?.capitalized ?? "") Timezone \(data["timezone"])"
                                    
                                    self.time = data["remote_time"].stringValue
                                    
                                    do{
                                        
                                        self.remote_attendace = try JSONDecoder().decode(RemoteData.self, from: data["remote_attendance"].rawData())
                                        
                                        self.isRemote = true
                                        print(self.remote_attendace)
                                        
                                    }
                                    catch{
                                        print("error catch ",error.localizedDescription)
                                    }
                                    
                                }
                                
                                
                                
                                self.dateServer.text = EmporeHelper.convertDateFormater(self.time ?? "2010-10-10 10:20:30")
                                
                                
                                if(data["absensi"]["clock_in"].string != nil) {
                                    self.statusAbsen = 1
                                    self.stck1.alpha = 1
                                    self.marginTopLine.constant = 120
                                    self.clockStatus.text = "You have clocked in today"
                                    
                                    self.btnClockIn.setTitle("Clock Out", for: .normal)
                                    self.btnClockIn.backgroundColor = .red
                                    self.btnClockIn.leftImage(image: #imageLiteral(resourceName: "ic_clock_out").sd_tintedImage(with: .white)!)
                                    
                                    self.clockIn.text = data["absensi"]["clock_in"].string ?? "--"
                                    self.clockOut.text = data["absensi"]["clock_out"].string ?? "--"
                                    self.duration.text = data["absensi"]["work_time"].string ?? "--"
                                    
                                    /// check color clock status
                                    
                                    if(data["absensi"]["late"].string == nil) {
                                        print("late ",data["absensi"]["late"].stringValue)
                                        self.clockIn.textColor = .primary
                                    }else {
                                        
                                        self.clockIn.textColor = .red
                                    }
                                    
                                    if(data["absensi"]["early"].string == nil) {
                                        
                                        self.clockOut.textColor = .primary
                                    }else {
                                        
                                        self.clockOut.textColor = .red
                                    }
                                    
                                    
                                    if data["absensi"]["clock_out"].string != nil {
                                        self.clockStatus.text = "Your attendance has been recorded"
                                        self.clockIn.text = data["absensi"]["clock_in"].string ?? "--"
                                        self.clockOut.text = data["absensi"]["clock_out"].string ?? "--"
                                        self.duration.text = data["absensi"]["work_time"].string ?? "--"
                                        
                                        self.btnClockIn.alpha = 0
                                        
                                        self.timer2.invalidate()
                                        
                                        self.bottomMargin.constant = -20
                                    }
                                    else {
                                        self.clockStatus.text = "You have not clocked out today"
                                        self.clockIn.text = data["absensi"]["clock_in"].string ?? "--"
                                        self.clockOut.text = data["absensi"]["clock_out"].string ?? "--"
                                        
                                        /// check dynamic duration
                                        
                                        self.clockIndate = data["absensi"]["updated_at"].string ?? ""
                                        
                                        
                                    }
                                    
                                }
                                else {
                                    self.clockStatus.text = "You have not clocked in today"
                                    self.statusAbsen = 0
                                    self.stck1.alpha = 0
                                    self.btnClockIn.leftImage(image: #imageLiteral(resourceName: "ic_clock_in").sd_tintedImage(with: .white)!)
                                    self.marginTopLine.constant = 30
                                    
                                }
                                
                                
                                
                            }
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            self.progressView.hide()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
        }
    }
    
    func getDataProfile(){
        
        progressView.show(self)
        
        getCode()
        getToken()
        
        let base_Url = Utils.base_url+"/profile?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        Alamofire.request(base_Url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error)
                    self.progressView.hide()
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.getDataProfile()
                            
                        }
                    }
                }else {
                    self.progressView.hide()
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            let responseStatus : JSON = JSON(response.result.value!)
                            let status = responseStatus["status"]
                            let data = responseStatus["data"]
                            
                            print("data profile ", responseStatus)
                            
                            if(status == "error"){
                                print("error ", response.result.value!)
                                let title = "Unstable Connection"
                                let message = "Please check your Connection & try again..."
                                self.showAlert(title: title, message: message)
                                
                            }else {
                                
                                do{
                                    
                                    var currentDateTime = Date()
                                    
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeStyle = .medium
                                    
                                    
                                    
                                    self.dataProfile = try JSONDecoder().decode(Profile.self, from: data.rawData())
                                    
                                    self.nameLabel.text = self.dataProfile?.user?.name
                                    self.companyName.text = self.dataProfile?.company?[0].value
                                    self.quotes.text = self.dataProfile?.company?[1].value
                                    
                                    let url_image = URL(string: Utils.base_url_image+(self.dataProfile?.company?[2].value)!)!
                                    
                                    self.img_Logo.sd_setImage(with: url_image, completed: nil)
                                    
                                    self.getClockStatus()
                                    
                                }
                                catch{
                                    print("error catch ",error.localizedDescription)
                                }
                                
                                
                            }
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            self.progressView.hide()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
        }
    }
    
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "hh:mm"
        
        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }
        
        //You can directly use from here if you have two dates
        
        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        return "\(intervalInt < 0 ? "-" : "+") \(Int(hour)) Hours \(Int(minute)) Minutes"
    }
    
    func validateToken() {
        
        print("cek token")
        progressView.show(self)
        
        getCode()
        getToken()
        
        let base_Url = Utils.base_url+"/validate?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        Alamofire.request(base_Url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error)
                    self.progressView.hide()
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.validateToken()
                            
                        }
                    }
                }else {
                    self.progressView.hide()
                    
                    let responseStatus : JSON = JSON(response.result.value!)
                    let status = responseStatus["status"]
                    print("data token ", responseStatus)
                    
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            
                            
                            if(status == "error"){
                                
                                print("logout")
                                let prefs = UserDefaults.standard
                                prefs.removeObject(forKey:"has_login")
                                
                                self.view.window?.rootViewController?.dismiss(animated: false, completion: nil)
                                
                            }else {
                                
                                print("token still valid")
                                
                            }
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            self.progressView.hide()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
        }
    }
    
    
    @IBAction func takePhoto(_ sender: UIButton) {
        
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.cameraDevice = .front
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.destination is AbsenController){
            let vc = segue.destination as? AbsenController
            vc?.imageAbsen = dataPhotoAbsen
            vc?.absenType = String(statusAbsen)
            vc?.canOutOfOffice = outOfOffice
            vc?.dataRemote = remote_attendace
            vc?.isRemote   = isRemote
            vc?.absenDelegate = self
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        //        img_Logo.image = info[.originalImage] as? UIImage
        dataPhotoAbsen = info[.originalImage] as Any
        
        performSegue(withIdentifier: "showAbsenView", sender: self)
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        
        setInputContainer()
        hideKeyboardWhenTappedAround()
        makeRadiusImage()
        makeBtnRounded()
    }
    
    func setInputContainer(){
        inputContainer.layer.cornerRadius = 10
        inputContainer.backgroundColor = UIColor(white: 1, alpha: 0.5)
        inputContainer.layer.borderWidth = 4
        inputContainer.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
    }
    
    func makeRadiusImage(){
        img_Logo?.layer.cornerRadius = (img_Logo?.frame.size.width ?? 0.0) / 2
        img_Logo?.clipsToBounds = true
        img_Logo?.layer.borderWidth = 2.0
        img_Logo?.layer.borderColor = UIColor.white.cgColor
    }
    
    func makeBtnRounded(){
        btnClockIn.layer.cornerRadius = 3
    }
    
    func setImageTitle(){
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "emhr_logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
}
