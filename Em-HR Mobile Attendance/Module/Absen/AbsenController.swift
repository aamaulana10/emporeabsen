//
//  AbsenController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 18/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import UserNotifications
import Alamofire
import SwiftyJSON
import MaterialComponents

protocol fromAbsenDelegate{
    
    func fromAbsen(data : Bool)
}

class AbsenController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate,
UINavigationControllerDelegate, UIImagePickerControllerDelegate, UNUserNotificationCenterDelegate{
    
    @IBOutlet weak var justificationField: MDCTextField!
    @IBOutlet weak var absenMap: MKMapView!
    var destination : MKMapItem?
    
    var activityView: UIActivityIndicatorView?
    
    var absenDelegate : fromAbsenDelegate?
    
    @IBOutlet weak var absenPhoto: UIImageView!
    @IBOutlet weak var userDistance: UILabel!
    @IBOutlet weak var marbotToStackView: NSLayoutConstraint!
    @IBOutlet weak var photoHeight: NSLayoutConstraint!
    @IBOutlet weak var mapHeight: NSLayoutConstraint!
    
    @IBOutlet weak var outChk: VKCheckbox!
    
    @IBOutlet weak var checkStack: UIStackView!
    @IBOutlet weak var btnClock: UIButton!
    var imagePicker: UIImagePickerController!
    var imageAbsen : Any = ""
    var absenType  =  ""
    var statusAbsen = "normal"
    var canOutOfOffice = ""
    
    var locationManager : CLLocationManager = CLLocationManager()
    var userLocation : CLLocation?
    
    var code = ""
    var token = ""
    
    var dataProfile : Profile?
    var dataRemote  : RemoteData?
    var distance    = 0
    var isRemote    = false
    
    var justMcd     : MDCTextInputControllerOutlined?
    
    var progressView = ProgressView()
    
    var timer        = Timer()
    var size         = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        absenPhoto.image = imageAbsen as? UIImage
        self.absenMap.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.delegate = self
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.badge, .alert, .sound]) { (granted, error) in
        }
        
        absenMap.showsUserLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestLocation()
        
        getDataProfile()
        
        locationManager.startUpdatingLocation()
        
        outChk.line             = .thin
        outChk.bgColorSelected  = UIColor(red: 46/255, green: 119/255, blue: 217/255, alpha: 1)
        outChk.bgColor          = .primary
        outChk.color            = UIColor.white
        outChk.borderColor      = UIColor.white
        outChk.borderWidth      = 1
        
        outChk.setOn(false)
        
        outOffice()
        
        outChk.checkboxValueChangedBlock = {
            isOn in
            self.outOffice()
        }
        
        justMcd     = FormHelper.textLayout(justificationField)
        btnClock.layer.cornerRadius = 5
        
        timer = Timer.scheduledTimer(timeInterval: 30.0,
                                     target: self,
                                     selector: #selector(updateUserLocation),
                                     userInfo: nil,
                                     repeats: true)
        
        if absenType == "1" {
            btnClock.backgroundColor = .red
            btnClock.setTitle("CLOCK OUT", for: .normal)
        }
        
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                // User Interface is Dark
                
                justificationField.textColor  = .white
            } else {
                // User Interface is Light
                justificationField.textColor = .black
            }
        } else {
            // Fallback on earlier versions
        }
        
        print("can out of office ",canOutOfOffice)
        
        if canOutOfOffice == "1" {
            checkStack.isHidden = false
            marbotToStackView.constant = 10
            statusAbsen = "out_of_office"
        }else {
            checkStack.isHidden = true
            marbotToStackView.constant = -10
            statusAbsen = "normal"
        }
        
        photoHeight.constant = size.height / 2 - 80
        mapHeight.constant   = size.height / 2 - 80
        
        
        print("is remote ", isRemote)
        print("absen type ", absenType)
        print("status absen ", statusAbsen)
        print("can out of office ", canOutOfOffice)
    }
    
    @objc func updateUserLocation() {
        getDirections()
    }
    
    @IBAction func updateBtn(_ sender: UIButton) {
        getDirections()
    }
    
    
    func outOffice(){
        
        if outChk.isOn {
            
            self.marbotToStackView.constant = 70
            self.justificationField.isHidden = false
            self.statusAbsen = "out_of_office"
        }else {
            
            self.marbotToStackView.constant = 10
            self.justificationField.isHidden = true
            self.statusAbsen = "normal"
        }
        
        print(self.statusAbsen)
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    @IBAction func cancelAbsen(_ sender: UIButton) {
        absenDelegate?.fromAbsen(data: true)
        dismiss(animated: true)
    }
    
    @IBAction func retakeAbsenPhoto(_ sender: UIButton) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.cameraDevice = .front
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Trait collection has already changed
        if #available(iOS 12.0, *) {
            let userInterfaceStyle = traitCollection.userInterfaceStyle
            
            if  userInterfaceStyle == .dark {
                // User Interface is Dark
                
                justificationField.textColor  = .white
            } else {
                // User Interface is Light
                justificationField.textColor = .black
            }
            
            
        } else {
            // Fallback on earlier versions
        }
    }
    
    func getCode(){
        if let companyCode = UserDefaults.standard.object(forKey: "code") as? String {
            code = companyCode
            
            print(code)
        }
        
    }
    
    func getToken(){
        if let dataToken = UserDefaults.standard.object(forKey: "token") as? String {
            token = dataToken
            
            print(token)
        }
        
    }
    
    func getDataProfile(){
        progressView.show(self)
        getCode()
        getToken()
        
        let base_Url = Utils.base_url+"/profile?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        Alamofire.request(base_Url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error)
                    self.progressView.hide()
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.getDataProfile()
                            
                        }
                    }
                }else {
                    self.progressView.hide()
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            let responseStatus : JSON = JSON(response.result.value!)
                            let status = responseStatus["status"]
                            let data = responseStatus["data"]
                            
                            print("data profile ", responseStatus)
                            
                            if(status == "error"){
                                print("error ", response.result.value!)
                                let title = "Unstable Connection"
                                let message = "Please check your Connection & try again..."
                                self.showAlert(title: title, message: message)
                                
                            }else {
                                
                                do{
                                    
                                    self.dataProfile = try JSONDecoder().decode(Profile.self, from: data.rawData())
                                    
                                    if(self.isRemote == false) {
                                        
                                        
                                        let info1 = CustomPointAnnotation()
                                        info1.coordinate = CLLocationCoordinate2DMake(Double(self.dataProfile?.user?.branch?.latitude ?? 0.0), Double(self.dataProfile?.user?.branch?.longitude ?? 0.0))
                                        info1.title = self.dataProfile?.user?.branch?.name
                                        info1.subtitle = "Company"
                                        info1.imageName = "pin2.png"
                                        
                                        self.absenMap.addAnnotation(info1)
                                        
                                        self.showCircle(coordinate: info1.coordinate, radius: Double(self.dataProfile?.user?.branch?.radius ?? 100))
                                        
                                    }else {
                                        
                                        let info1 = CustomPointAnnotation()
                                        info1.coordinate = CLLocationCoordinate2DMake(Double(self.dataRemote?.latitude ?? 0.0), Double(self.dataRemote?.longitude ?? 0.0))
                                        info1.title = self.dataRemote?.location_name
                                        info1.subtitle = "Remote Location"
                                        info1.imageName = "pin2.png"
                                        
                                        self.absenMap.addAnnotation(info1)
                                        
                                        self.showCircle(coordinate: info1.coordinate, radius: Double(self.dataRemote?.radius ?? 100))
                                    }
                                    
                                    self.getDirections()
                                    
                                    
                                }
                                catch{
                                    print("error catch ",error.localizedDescription)
                                }
                                
                                
                            }
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            //                            self.hideActivityIndicator()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
        }
    }
    
    func absen(){
        
        progressView.show(self)
        getCode()
        getToken()
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        let base_Url = Utils.base_url+"/clock?company=" + code
        
        let imageData: UIImage? = absenPhoto.image
        
        let uploadDict = ["latitude":"\(userLocation?.coordinate.latitude ?? 0)", "longitude": "\(userLocation?.coordinate.longitude ?? 0)", "type":absenType,"status":statusAbsen, "justification" : String(justificationField.text ?? "")] as [String:String]
        
        print("uploadDict ",uploadDict)
        
        let resizedImage = imageData?.resizedImageWithinRect(rectSize: CGSize(width: (imageData?.size.width)! / 6, height: (imageData?.size.height)! / 6))

        
        Alamofire.upload(multipartFormData: { MultipartFormData in

            if let image :Data = resizedImage?.jpegData(compressionQuality: 0.5) {
                MultipartFormData.append(image, withName: "foto" , fileName: "foto.jpeg" , mimeType: "image/jpeg")
                for(key,value) in uploadDict{

                    MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)

                }
            }


        }, to: base_Url, headers: headersku , encodingCompletion: {
            EncodingResult in
            switch EncodingResult{
            case .success(let upload, _, _):
                upload.responseJSON { response in

                    print(response)

                    self.progressView.hide()

                    let responseStatus : JSON = JSON(response.result.value!)
                    let status = responseStatus["status"]

                    print("data profile ", responseStatus)

                    if(status == "error"){
                        print("error ", response.result.value!)
                        let title = "Unstable Connection"
                        let message = "Please check your Connection & try again..."
                        self.showAlert(title: title, message: message)

                    }else {

                        let title = "Success"
                        let message = responseStatus["message"].stringValue
                        self.showAlertSuksesAbsen(title: title, message: message)

                    }

                }
            case .failure(let encodingError):

                self.progressView.hide()

                print("ERROR RESPONSE: \(encodingError)")
                let title = "Service Unavailable"
                let message = "Please try again letter..."
                self.showAlert(title: title, message: message)


            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        absenDelegate?.fromAbsen(data: true)
        if let firstVC = presentingViewController as? BerandaController {
            DispatchQueue.main.async {
                firstVC.getDataProfile()
                firstVC.getClockStatus()
            }
        }
    }
    
    
    @IBAction func clockINAbsen(_ sender: UIButton) {
        if outChk.isOn {
            if(justificationField.text != ""){
                
                absen()
                print("is on dan boleh absen")
            }else {
                
                let title = absenType == "0" ? "Clock In fail" : "Clock out fail"
                let message = "Please fill in the justification field for attendance outside the office"
                self.showAlert(title: title, message: message)
            }
        }else {
            
            if (isRemote == false){
                
                if(distance > dataProfile?.user?.branch?.radius ?? 100){
                    print("lebih dari \(dataProfile?.user?.branch?.radius ?? 100) berarti belum boleh absen")
                    let title = absenType == "0" ? "Clock In fail" : "Clock out fail"
                    let message = "You must be less than \(dataProfile?.user?.branch?.radius ?? 100) m away for \(absenType == "0" ? "clock in" : "clock out")"
                    self.showAlert(title: title, message: message)
                }else {
                    absen()
                    print("distance no remote ", distance)
                    print("absen bisa no remote")
                }
            }else {
                
                if(distance > dataRemote?.radius ?? 100){
                    print("lebih dari \(dataRemote?.radius ?? 100) berarti belum boleh absen")
                    let title = absenType == "0" ? "Clock In fail" : "Clock out fail"
                    let message = "You must be less than \(dataRemote?.radius ?? 100) m away for \(absenType == "0" ? "clock in" : "clock out")"
                    self.showAlert(title: title, message: message)
                }else {
                    absen()
                    print("distance remote ",distance)
                    print("absen bisa remote")
                }
            }
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.authorizedAlways) {
            print("authorize")
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        userLocation = locations[0]
        
        print("latitude ", userLocation?.coordinate.latitude)
        print("longitude ", userLocation?.coordinate.longitude)
        
        self.getDirections()
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            //return nil so map view draws "blue dot" for standard user location
            
            let pin = mapView.view(for: annotation) as? MKPinAnnotationView ?? MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
            pin.image = UIImage(named: "ic_man")
            return pin
            
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.canShowCallout = true
        }
        else {
            anView?.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let cpa = annotation as! CustomPointAnnotation
        
        //        anView?.image = UIImage(named:cpa.imageName)
        //        let transform = CGAffineTransform(scaleX: 0.15, y: 0.15)
        //        anView?.transform = transform
        
        let image = UIImage(named: "pin2.png")
        let resizedSize = CGSize(width: 50, height: 50)
        
        UIGraphicsBeginImageContext(resizedSize)
        image?.draw(in: CGRect(origin: .zero, size: resizedSize))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        anView?.image = resizedImage
        
        
        
        
        return anView
    }
    
    func getDirections() {
        let markSecondPoint = MKPlacemark(coordinate: CLLocationCoordinate2DMake(Double(self.dataProfile?.user?.branch?.latitude ?? 0.0), Double(self.dataProfile?.user?.branch?.longitude ?? 0.0)), addressDictionary: nil)
        
        let markSecondPointRemote = MKPlacemark(coordinate: CLLocationCoordinate2DMake(Double(self.dataRemote?.latitude ?? 0.0), Double(self.dataRemote?.longitude ?? 0.0)), addressDictionary: nil)
        let request = MKDirections.Request()
        
        
        if(isRemote == false){
            
            request.destination = MKMapItem(placemark: markSecondPoint)
        }else {
            
            request.destination = MKMapItem(placemark: markSecondPointRemote)
        }
        
        
        
        request.source = MKMapItem.forCurrentLocation()
        
        request.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: request)
        
        directions.calculate(completionHandler: {(response, error) in
            
            if let error = error {
                print(error.localizedDescription)
            } else {
                if let response = response {
                    guard error == nil, let route = response.routes.first else {return}
                    print(response)
                    //                    self.distance = Int(route.distance)
                    let coordinateDevice = CLLocation(latitude: self.userLocation?.coordinate.latitude ?? 0.0, longitude: self.userLocation?.coordinate.longitude ?? 0.0)
                    let coordinateUser = CLLocation(latitude: Double(self.dataProfile?.user?.branch?.latitude ?? 0.0), longitude: Double(self.dataProfile?.user?.branch?.longitude ?? 0.0))
                     let coordinateRemote = CLLocation(latitude: Double(self.dataRemote?.latitude ?? 0.0), longitude: Double(self.dataRemote?.longitude ?? 0.0))
                    let distanceInMeters = coordinateDevice.distance(from: self.isRemote == false ? coordinateUser : coordinateRemote)
                    
                    self.distance = Int(distanceInMeters)
                    
//                    print("jarak:\(route.distance) meters")
                    self.userDistance.numberOfLines = 0
                    self.userDistance.text = "Your location \(self.isRemote == true ? "(REMOTE)" : "is \(self.distance) meters from the office")"
                    
                    print("distance saat ini ", self.distance)
                    print("distance saat ini baru ", distanceInMeters)
                    
                    self.locationManager.stopUpdatingLocation()
                    self.showRoute(response)
                }
            }
        })
    }
    
    func showRoute(_ response: MKDirections.Response) {
        
        for route in response.routes {
            
            absenMap.addOverlay(route.polyline,
                                level: MKOverlayLevel.aboveRoads)
            
            for step in route.steps {
                print(step.instructions)
            }
        }
        
        if let coordinate = userLocation?.coordinate {
            let region =
                MKCoordinateRegion(center: coordinate,
                                   latitudinalMeters: 4000, longitudinalMeters: 4000)
            absenMap.setRegion(region, animated: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func createLocalNotification(message: String, identifier: String) {
        //Create a local notification
        let content = UNMutableNotificationContent()
        content.body = message
        content.sound = UNNotificationSound.default
        
        // Deliver the notification
        let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: nil)
        
        // Schedule the notification
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error) in
        }
    }
    
    func showCircle(coordinate: CLLocationCoordinate2D,
                    radius: CLLocationDistance) {
        let circle = MKCircle(center: coordinate,
                              radius: radius)
        absenMap.addOverlay(circle)
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        
        if let circleOverlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: circleOverlay)
            circleRenderer.fillColor = UIColor(red: 0.0/255.0, green: 203.0/255.0, blue: 208.0/255.0, alpha: 0.7)
            
            circleRenderer.strokeColor = UIColor(red: 7.0/255.0, green: 106.0/255.0, blue: 255.0/255.0, alpha: 1)
            
            circleRenderer.lineWidth = 4.0
            
            return circleRenderer
        }
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        renderer.strokeColor = .primary
        renderer.lineWidth = 5.0
        return renderer
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        absenPhoto.image = info[.originalImage] as? UIImage
        imageAbsen = info[.originalImage] as Any
        
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func showAlertSuksesAbsen(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            
            self.locationManager.stopUpdatingLocation()
            self.timer.invalidate()
            self.timer = Timer()
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true)
    }
    
    
}

class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}
