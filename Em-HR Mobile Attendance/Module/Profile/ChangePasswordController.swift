//
//  ChangePasswordController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MaterialComponents

class ChangePasswordController: UIViewController {
    
    var activityView: UIActivityIndicatorView?
    var screenSize = UIScreen.main.bounds
    
    var currentMcd  : MDCTextInputControllerOutlined?
    var newMcd : MDCTextInputControllerOutlined?
    
    var code = ""
    var token = ""
    
    var progressView = ProgressView()
    
    @IBOutlet weak var currentPasswordLabel: MDCTextField!
    @IBOutlet weak var newPasswordLabel: MDCTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         currentMcd      = FormHelper.textLayout(currentPasswordLabel)
         newMcd          = FormHelper.textLayout(newPasswordLabel)
         EmporeHelper.setPasswordEye(self, currentPasswordLabel, #selector(self.currentPwdListener(_:)))
         EmporeHelper.setPasswordEye(self, newPasswordLabel, #selector(self.newPwdListener(_:)))
        
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                // User Interface is Dark
                
                currentPasswordLabel.textColor  = .black
                newPasswordLabel.textColor    = .black
            } else {
                // User Interface is Light
                 currentPasswordLabel.textColor = .black
                newPasswordLabel.textColor    = .black
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func currentPwdListener(_ sender: UIButton){
        EmporeHelper.passwordEyeTextHide(sender, currentPasswordLabel)
    }
    
    @objc func newPwdListener(_ sender: UIButton){
        EmporeHelper.passwordEyeTextHide(sender, newPasswordLabel)
    }
    
    func getCode(){
        if let companyCode = UserDefaults.standard.object(forKey: "code") as? String {
            code = companyCode
            
            print(code)
        }
        
    }
    
    func getToken(){
        if let dataToken = UserDefaults.standard.object(forKey: "token") as? String {
            token = dataToken
            
            print(token)
        }
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Trait collection has already changed
        if #available(iOS 12.0, *) {
            let userInterfaceStyle = traitCollection.userInterfaceStyle
            
            if  userInterfaceStyle == .dark {
                // User Interface is Dark
                
                newPasswordLabel.textColor  = .black
                currentPasswordLabel.textColor    = .black
            } else {
                // User Interface is Light
                newPasswordLabel.textColor = .black
                currentPasswordLabel.textColor    = .black
            }

            
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func updatePassword(_ sender: UIButton) {
        
        update()
        
    }
    
    func update(){
        progressView.show(self)
        
        getCode()
        getToken()
        
        let base_Url = Utils.base_url+"/update_password?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        let update_params : [String : Any] = [
            "password" : currentPasswordLabel.text ?? "",
            "new_password" : newPasswordLabel.text ?? ""
        ]
        
        Alamofire.request(base_Url, method: .post, parameters: update_params, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error)
                    
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.update()
                            
                        }
                    }
                }else {
                      self.progressView.hide()
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            let responseStatus : JSON = JSON(response.result.value!)
                            let status = responseStatus["status"]
                            
                            if(status == "error"){
                                print("error ", response.result.value!)
                                  self.progressView.hide()
                                let title = "Error when update Password"
                                let message = "Please check your data & try again..."
                                self.showAlert(title: title, message: message)
                                
                            }else {
                                
                                print("respon ",responseStatus)
                                
                                    
                                    let title = "Success update Password"
                                    let message = "Your Password has been updated"
                                    self.showAlert(title: title, message: message)
                               
                            }
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                              self.progressView.hide()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
                
        }
    }
    
    
    
    @IBAction func cancelUpdate(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.currentPasswordLabel.text = ""
            self.newPasswordLabel.text = ""
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true)
    }
}
