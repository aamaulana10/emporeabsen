//
//  ProfileShiftController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 15/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProfileShiftController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var cellName = "ShiftCell"
    var screenSize = UIScreen.main.bounds
    
    var code = ""
    var token = ""
    
    var dataShift : ShiftDetail1?
    
    var activityView: UIActivityIndicatorView?
    
    var progressView = ProgressView()
    
    @IBOutlet weak var ShiftDetail: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTableView()
        getShiftDetail()
        
    }
    
    func getCode(){
        if let companyCode = UserDefaults.standard.object(forKey: "code") as? String {
            code = companyCode
            
            print(code)
        }
        
    }
    
    func getToken(){
        if let dataToken = UserDefaults.standard.object(forKey: "token") as? String {
            token = dataToken
            
            print(token)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataShift?.detail?.count ?? 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ShiftCell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath) as! ShiftCell
        
        let data = dataShift?.detail?[indexPath.row]
        
        cell.day.text = data?.day
        cell.clockIn.text = data?.clock_in ?? "--:--"
        cell.clockOut.text = data?.clock_out ?? "--:--"
        //        cell.duration.text = ""
        
        return cell
    }
    
    func getShiftDetail(){
        
        progressView.show(self)
        
        getCode()
        getToken()
        
        let base_Url = Utils.base_url+"/shift_detail?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        Alamofire.request(base_Url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error)
                    //                    self.hideActivityIndicator()
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.getShiftDetail()
                            
                        }
                    }
                }else {
                      self.progressView.hide()
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            let responseStatus : JSON = JSON(response.result.value!)
                            let status = responseStatus["status"]
                            let data = responseStatus["data"]
                            
                            if(status == "error"){
                                print("error ", response.result.value!)
                                let title = "Unstable Connection"
                                let message = "Please check your Connection & try again..."
                                self.showAlert(title: title, message: message)
                                
                            }else {
                                
                                print("respon Shift Detail ",data)
                                
                                do{
                                    
                                    self.dataShift = try JSONDecoder().decode(ShiftDetail1.self, from: data.rawData())
                                    
                                    self.ShiftDetail.reloadData()
                                    
                                }
                                catch{
                                    print("error catch ",error)
                                }
                            }
                            
                        case .failure(let error):
                            print(error)
                              self.progressView.hide()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
        }
        
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    
    
    func configTableView(){
        ShiftDetail.delegate = self
        ShiftDetail.dataSource = self
        
        ShiftDetail.register(UINib.init(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        
        //riwayatTable.rowHeight = screenSize.height / 8
        ShiftDetail.backgroundColor = .clear
        ShiftDetail.separatorStyle = .none
    }
    
    
    
}
