//
//  ProfileMapController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 15/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import MapKit

class ProfileMapController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var userMap: MKMapView!
    
    var locationManager : CLLocationManager = CLLocationManager()
    
    var latitude = 0.0
    var longitude = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userMap.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.delegate = self
        
        
        self.setUpGeofenceForPlayaGrandeBeach()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        tabBarController?.tabBar.isHidden = false
        
    }
    
    func setUpGeofenceForPlayaGrandeBeach() {
        let geofenceRegionCenter = CLLocationCoordinate2DMake(latitude, longitude);
        let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: 100, identifier: "CurrentLocation1");
        geofenceRegion.notifyOnExit = true;
        geofenceRegion.notifyOnEntry = true;
        
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let mapRegion = MKCoordinateRegion(center: geofenceRegionCenter, span: span)
        self.userMap.setRegion(mapRegion, animated: true)
        
        let regionCircle = MKCircle(center: geofenceRegionCenter, radius: 400)
        self.userMap.addOverlay(regionCircle)
        //        self.userMap.showsUserLocation = true;
        
        let info1 = CustomPointAnnotation()
        info1.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        info1.title = "Office Location"
        info1.subtitle = "Company"
        info1.imageName = "pin2.png"
        
        self.userMap.addAnnotation(info1)
        
        self.locationManager.startMonitoring(for: geofenceRegion)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.authorizedAlways) {
            self.setUpGeofenceForPlayaGrandeBeach()
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.canShowCallout = true
        }
        else {
            anView?.annotation = annotation
        }
        
        let image = UIImage(named: "pin2.png")
        let resizedSize = CGSize(width: 50, height: 50)
        
        UIGraphicsBeginImageContext(resizedSize)
        image?.draw(in: CGRect(origin: .zero, size: resizedSize))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        anView?.image = resizedImage
        
        return anView
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        //Good place to schedule a local notification
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        //Good place to schedule a local notification
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let overlayRenderer : MKCircleRenderer = MKCircleRenderer(overlay: overlay);
        overlayRenderer.lineWidth = 4.0
        overlayRenderer.strokeColor = UIColor(red: 7.0/255.0, green: 106.0/255.0, blue: 255.0/255.0, alpha: 0.5)
        overlayRenderer.fillColor = UIColor(red: 0.0/255.0, green: 203.0/255.0, blue: 208.0/255.0, alpha: 0.3)
        return overlayRenderer
    }
    
}
