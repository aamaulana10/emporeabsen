//
//  ProfileController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SwiftyJSON
import SDWebImage

class ProfileController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var profileTable: UITableView!
    weak var parallaxHeaderView: UIView?
    
    var activityView: UIActivityIndicatorView?
    
    var cellName = "ProfileCell"
    var screenSize = UIScreen.main.bounds
    
    var progressView = ProgressView()
    
    var code = ""
    var token = ""
    
    var dataProfile : Profile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configView()
        setupProfileTable()
        getDataProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        setImageTitle()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func logOut(_ sender: UIButton) {
        
        let title     = "Confirmation"
        let message   = "Do you want to logout ?"
        
        showAlertConfirmation(title: title, message: message)
        
    }
    
    
    func getCode(){
        if let companyCode = UserDefaults.standard.object(forKey: "code") as? String {
            code = companyCode
            
            print(code)
        }
        
    }
    
    func getToken(){
        if let dataToken = UserDefaults.standard.object(forKey: "token") as? String {
            token = dataToken
            
            print(token)
        }
        
    }
    
    
    func getDataProfile(){
        
        progressView.show(self)
        
        getCode()
        getToken()
        
        let base_Url = Utils.base_url+"/profile?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        Alamofire.request(base_Url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error)
                    self.progressView.hide()
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.getDataProfile()
                            
                        }
                    }
                }else {
                    self.progressView.hide()
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            let responseStatus : JSON = JSON(response.result.value!)
                            let status = responseStatus["status"]
                            let data = responseStatus["data"]
                            
                            print("data profile ", responseStatus)
                            
                            if(status == "error"){
                                print("error ", response.result.value!)
                                let title = "Unstable Connection"
                                let message = "Please check your Connection & try again..."
                                self.showAlert(title: title, message: message)
                                
                            }else {
                                
                                do{
                                    
                                    self.dataProfile = try JSONDecoder().decode(Profile.self, from: data.rawData())
                                    self.setupParallaxHeader()
                                    self.profileTable.reloadData()
                                    
                                }
                                catch{
                                    print("error catch ",error.localizedDescription)
                                }
                                
                                
                            }
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            self.progressView.hide()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
        }
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func showAlertConfirmation(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (action: UIAlertAction!) in
            
            print("logout")
            let prefs = UserDefaults.standard
            prefs.removeObject(forKey:"has_login")
            
            self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    private func setupParallaxHeader() {
        let size = UIScreen.main.bounds
        
        let url_image = URL(string: Utils.base_url_image + "/storage/foto/"+(dataProfile?.user?.foto ?? ""))
        
        
        print(url_image)
        
        let imageView = UIImageView()
        imageView.sd_setImage(with: url_image, placeholderImage: UIImage.init(imageLiteralResourceName: "user"), options: [], completed: nil)
        imageView.contentMode = .scaleAspectFill
        parallaxHeaderView = imageView
        
        //setup bur view
        imageView.blurView.setup(style: UIBlurEffect.Style.dark, alpha: 0).enable()
        
        profileTable.parallaxHeader.view = imageView
        profileTable.parallaxHeader.height = size.height / 3
        profileTable.parallaxHeader.minimumHeight = 120
        profileTable.parallaxHeader.mode = .centerFill
        profileTable.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
            //update alpha of blur view on top of image view
            parallaxHeader.view.blurView.alpha = 1 - parallaxHeader.progress
        }
        
        let roundIcon = UIImageView(
            frame: CGRect(x: 0, y: 0, width: 100, height: 100)
        )
        roundIcon.sd_setImage(with: url_image, placeholderImage: UIImage.init(imageLiteralResourceName: "user"), options: [], completed: nil)
        roundIcon.layer.borderColor = UIColor.white.cgColor
        roundIcon.layer.borderWidth = 2
        roundIcon.contentMode = .scaleAspectFill
        roundIcon.layer.cornerRadius = roundIcon.frame.width / 2
        roundIcon.clipsToBounds = true
        
        //add round image view to blur content view
        //do not use vibrancyContentView to prevent vibrant effect
        imageView.blurView.blurContentView?.addSubview(roundIcon)
        //add constraints using SnpaKit library
        roundIcon.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.height.equalTo(100)
        }
        
    }
    
    
    //MARK: table view data source/delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProfileCell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath) as! ProfileCell
        
        cell.containerHeight.constant = screenSize.height / 17
        if(indexPath.row == 0){
            cell.titleLabel.text = "Profile"
            cell.subTitleLabel.text = ""
            cell.backgroundColor = .groupTableViewBackground
            cell.CONTAINER.backgroundColor = .groupTableViewBackground
        }
        else if(indexPath.row == 1){
            cell.titleLabel.text = "Name"
            cell.subTitleLabel.text = dataProfile?.user?.name?.uppercased()
            cell.backgroundColor = UIColor(hexString: "#ffffff")
            cell.CONTAINER.backgroundColor = UIColor(hexString: "#ffffff")
        }
        else if(indexPath.row == 2){
            cell.titleLabel.text = "Employee ID"
            cell.subTitleLabel.text = dataProfile?.user?.nik
            cell.backgroundColor = UIColor(hexString: "#ffffff")
            cell.CONTAINER.backgroundColor = UIColor(hexString: "#ffffff")
        }
        else if(indexPath.row == 3){
            cell.titleLabel.text = "E-mail"
            cell.subTitleLabel.text = dataProfile?.user?.email
            cell.backgroundColor = UIColor(hexString: "#ffffff")
            cell.CONTAINER.backgroundColor = UIColor(hexString: "#ffffff")
        }
        else if(indexPath.row == 4){
            cell.titleLabel.text = "Position"
            cell.subTitleLabel.text = dataProfile?.user?.position?.capitalized
            cell.backgroundColor = UIColor(hexString: "#ffffff")
            cell.CONTAINER.backgroundColor = UIColor(hexString: "#ffffff")
        }
        else if(indexPath.row == 5){
            cell.titleLabel.text = "Division"
            cell.subTitleLabel.text = dataProfile?.user?.division?.uppercased()
            cell.backgroundColor = UIColor(hexString: "#ffffff")
            cell.CONTAINER.backgroundColor = UIColor(hexString: "#ffffff")
        }
        else if(indexPath.row == 6){
            let passWord = "12345678"
            cell.titleLabel.text = "Change Password"
            cell.titleLabel.textColor = .primary
            cell.subTitleLabel.text = String(passWord.map { _ in return "•" })
            cell.subTitleLabel.textColor = .primary
            cell.backgroundColor = UIColor(hexString: "#ffffff")
            cell.CONTAINER.backgroundColor = UIColor(hexString: "#ffffff")
        }
        else if(indexPath.row == 7){
            cell.titleLabel.text = "Company"
            cell.subTitleLabel.text = ""
            cell.backgroundColor = .groupTableViewBackground
            cell.CONTAINER.backgroundColor = .groupTableViewBackground
        }
        else if(indexPath.row == 8){
            cell.titleLabel.text = "Name"
            cell.titleLabel.textColor = .black
            cell.subTitleLabel.textColor = .black
            cell.subTitleLabel.text = dataProfile?.company?[0].value?.uppercased()
            cell.backgroundColor = .white
            cell.CONTAINER.backgroundColor = .white
        }
        else if(indexPath.row == 9){
            // Create Attachment
            let imageAttachment = NSTextAttachment()
            imageAttachment.image = UIImage(named:"pin")
            // Set bound to reposition
            let imageOffsetY: CGFloat = -5.0
            imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 24, height: 24)
            // Create string with attachment
            let attachmentString = NSAttributedString(attachment: imageAttachment)
            // Initialize mutable string
            let completeText = NSMutableAttributedString(string: "")
            // Add image to mutable string
            completeText.append(attachmentString)
            // Add your text to mutable string
            let textAfterIcon = NSAttributedString(string: " \(String(describing: dataProfile?.user?.branch?.name?.capitalized ?? ""))")
            completeText.append(textAfterIcon)
            //            self.mobileLabel.textAlignment = .center
            //            self.mobileLabel.attributedText = completeText
            cell.titleLabel.text = "Branch"
            cell.titleLabel.textColor = .primary
            //            cell.subTitleLabel.text = dataProfile?.user?.branch?.name?.capitalized
            cell.subTitleLabel.attributedText = completeText
            cell.subTitleLabel.textColor = .primary
            cell.backgroundColor = UIColor(hexString: "#ffffff")
            cell.CONTAINER.backgroundColor = UIColor(hexString: "#ffffff")
        }
        else {
            cell.titleLabel.text = "Shift"
            cell.titleLabel.textColor = .primary
            cell.subTitleLabel.text = dataProfile?.user?.shift?.name
            cell.subTitleLabel.textColor = .primary
            cell.backgroundColor = UIColor(hexString: "#ffffff")
            cell.CONTAINER.backgroundColor = UIColor(hexString: "#ffffff")
        }
        
        return cell
        
    }
    
    func adaptivePresentationStyle(
        for controller: UIPresentationController,
        traitCollection: UITraitCollection)
        -> UIModalPresentationStyle {
            return .none
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 9){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "gotoMap") as! ProfileMapController
            vc.latitude = Double(dataProfile?.user?.branch?.latitude ?? 0.0)
            vc.longitude = Double(dataProfile?.user?.branch?.longitude ?? 0.0)
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if(indexPath.row == 6){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "gotoChangePassword") as! ChangePasswordController
            
            vc.preferredContentSize = CGSize(width: screenSize.width - 50, height: screenSize.height / 4 + 100)
            
            vc.modalPresentationStyle = .popover
            
            
            let popOver = vc.popoverPresentationController
            popOver?.delegate = self
            
            popOver?.permittedArrowDirections = .init(rawValue: 0)
            popOver?.sourceView = self.view
            
            let rect = CGRect(
                origin: CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2),
                size: CGSize(width: 1, height: 1)
            )
            popOver?.sourceRect = rect
            
            self.present(vc, animated: true, completion: nil)
            
            
            
        }
        
        if(indexPath.row == 10){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "gotoShift") as! ProfileShiftController
            
            vc.preferredContentSize = CGSize(width: screenSize.width - 50, height: screenSize.height / 2 + 100)
            
            vc.modalPresentationStyle = .popover
            
            
            let popOver = vc.popoverPresentationController
            popOver?.delegate = self
            
            popOver?.permittedArrowDirections = .init(rawValue: 0)
            popOver?.sourceView = self.view
            
            
            let rect = CGRect(
                origin: CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2),
                size: CGSize(width: 1, height: 1)
            )
            popOver?.sourceRect = rect
            
            if(dataProfile?.user?.shift?.name == nil) {
                
                let title   = "Shift Info"
                let message = "No shift time found"
                
                showAlert(title: title, message: message)
            }else {
                
                self.present(vc, animated: true, completion: nil)
            }
            
            
            
        }
    }
    
    func setupProfileTable(){
        profileTable.delegate = self
        profileTable.dataSource = self
        profileTable.register(UINib.init(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
    }
    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        
        hideKeyboardWhenTappedAround()
        
    }
    
    func setImageTitle(){
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "emhr_logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
}
