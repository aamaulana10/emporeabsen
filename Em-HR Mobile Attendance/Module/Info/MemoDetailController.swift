//
//  MemoDetailController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 13/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit

class MemoDetailController: UIViewController {
    
    var memoImageUrl = ""
    var memoTitle = ""
    var memoContent = ""
    var memoAuhtor = ""
    var memoDate = ""
    
    let baseUrlImage = "\(Utils.base_url_image)storage/internal-memo/"
    
    @IBOutlet weak var memoImage: UIImageView!
    @IBOutlet weak var memoPenulis: UILabel!
    @IBOutlet weak var memoIsi: UILabel!
    @IBOutlet weak var memoTanggal: UILabel!
    @IBOutlet weak var memoJudul: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title              = memoTitle
        setupDetail()
        //        configView()
        
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                // User Interface is Dark
                
                memoJudul.textColor  = .black
                memoIsi.textColor    = .black
            } else {
                // User Interface is Light
                memoJudul.textColor = .black
                memoIsi.textColor    = .black
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        EmporeHelper.setLightNav(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EmporeHelper.setLightNav(self)
    }
    
    func setupDetail(){
        let url_image = URL(string: baseUrlImage + memoImageUrl)
        memoImage.sd_setImage(with: url_image, placeholderImage: UIImage.init(imageLiteralResourceName: "noimage"), options: [], completed: nil)
        
        memoJudul.text = memoTitle
        memoPenulis.text = "By \(memoAuhtor)"
        memoTanggal.text = memoDate
        memoIsi.setHTMLFromString(htmlText: memoContent)
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Trait collection has already changed
        if #available(iOS 12.0, *) {
            let userInterfaceStyle = traitCollection.userInterfaceStyle
            
            if  userInterfaceStyle == .dark {
                // User Interface is Dark
                
                memoJudul.textColor  = .black
                memoIsi.textColor    = .black
            } else {
                // User Interface is Light
                memoJudul.textColor = .black
                memoIsi.textColor    = .black
            }
            
            
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        
        hideKeyboardWhenTappedAround()
        
    }
    
    func setImageTitle(){
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "emhr_logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
