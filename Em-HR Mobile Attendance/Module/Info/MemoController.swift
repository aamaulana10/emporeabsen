//
//  MemoController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 11/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import XLPagerTabStrip

class MemoController: UIViewController, UITableViewDataSource, UITableViewDelegate, IndicatorInfoProvider {
    
    var tabTitle        = ""
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: tabTitle)
    }
    
    @IBOutlet weak var MemoTable: UITableView!
    
    var activityView: UIActivityIndicatorView?
    
    var progressView = ProgressView()
    
    var cellHeaderName = "MemoHeaderCell"
    var cellName = "MemoCell"
    var screenSize = UIScreen.main.bounds
    
    var code = ""
    var token = ""
    
    var dataMemo : [Memo]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTableView()
        configView()
        getMemo()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        EmporeHelper.setDarkNav(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EmporeHelper.setDarkNav(self)
    }
    
    func getCode(){
        if let companyCode = UserDefaults.standard.object(forKey: "code") as? String {
            code = companyCode
            
            print(code)
        }
        
    }
    
    func getToken(){
        if let dataToken = UserDefaults.standard.object(forKey: "token") as? String {
            token = dataToken
            
            print(token)
        }
        
    }
    
    func getMemo(){
        progressView.show(self)
        
        getCode()
        getToken()
        
        let base_Url = Utils.base_url+"/memo?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        Alamofire.request(base_Url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error)
                    //                    self.hideActivityIndicator()
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.getMemo()
                            
                        }
                    }
                }else {
                    self.progressView.hide()
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            let responseStatus : JSON = JSON(response.result.value!)
                            let status = responseStatus["status"]
                            let data = responseStatus["data"]
                            
                            if(status == "error"){
                                print("error ", response.result.value!)
                                let title = "Unstable Connection"
                                let message = "Please check your Connection & try again..."
                                self.showAlert(title: title, message: message)
                                
                            }else {
                                
                                print("respon memo ",data)
                                
                                do{
                                    
                                    self.dataMemo = try JSONDecoder().decode([Memo].self, from: data.rawData())
                                    
                                    self.MemoTable.reloadData()
                                    
                                }
                                catch{
                                    print("error catch ",error)
                                }
                            }
                            
                        case .failure(let error):
                            print(error)
                            self.progressView.hide()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
        }
        
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    
    
    func configTableView(){
        MemoTable.delegate = self
        MemoTable.dataSource = self
        
        MemoTable.register(UINib.init(nibName: cellHeaderName, bundle: nil), forCellReuseIdentifier: cellHeaderName)
        MemoTable.register(UINib.init(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        
        //riwayatTable.rowHeight = screenSize.height / 8
        MemoTable.backgroundColor = .clear
        MemoTable.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataMemo?.count ?? 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MemoCell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath) as! MemoCell
        
        let cellHeader : MemoHeaderCell = tableView.dequeueReusableCell(withIdentifier: cellHeaderName, for: indexPath) as! MemoHeaderCell
        
        let data = dataMemo?[indexPath.row]
        
        let baseUrlImage = "\(Utils.base_url_image)storage/internal-memo/"
        
        let url_image = URL(string: baseUrlImage + (data?.thumbnail ?? ""))
        
        if(indexPath.row == 0){
            cellHeader.textHeader.text = data?.title
            cellHeader.headerSubTitle.text = data?.updated_at
            cellHeader.headerImage.sd_setImage(with: url_image, completed: nil)
            //            cellHeader.blurBg.backgroundColor = .gray
            //            cellHeader.blurBg.alpha = 0.2
            cellHeader.container.layer.cornerRadius = 1
            cellHeader.container.backgroundColor = UIColor(white: 1, alpha: 0.5)
            cellHeader.containerHeight.constant = screenSize.height / 3
            //        cell.backgroundColor = .clear
            //        cell.selectionStyle = .none
            return cellHeader
        }else {
            cell.titleCell.text = data?.title
            cell.subTitle.text = data?.updated_at
            cell.newsThumbnail.sd_setImage(with: url_image, placeholderImage: UIImage.init(imageLiteralResourceName: "noimage"), options: [], completed: nil)
            cell.imageWidth.constant = screenSize.width / 4 - 10
            cell.newsThumbnail.layer.cornerRadius = 2
            cell.Container.layer.cornerRadius = 1
            cell.Container.backgroundColor = UIColor(white: 1, alpha: 1)
            cell.ContainerHeight.constant = screenSize.height / 8
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "memoDetail") as? MemoDetailController
        
        vc?.memoImageUrl = dataMemo?[indexPath.row].thumbnail ?? ""
        vc?.memoTitle = dataMemo?[indexPath.row].title ?? ""
        vc?.memoAuhtor = dataMemo?[indexPath.row].author?.name ?? ""
        vc?.memoDate = dataMemo?[indexPath.row].updated_at ?? ""
        vc?.memoContent = dataMemo?[indexPath.row].content ?? ""
        
        EmporeHelper.pushWithHideBottomBar(self, vc!)
    }
    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        
        hideKeyboardWhenTappedAround()
        
    }
    
    func setImageTitle(){
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "emhr_logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
}
