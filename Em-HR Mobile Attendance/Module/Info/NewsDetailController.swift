//
//  NewsDetailController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 13/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit

class NewsDetailController: UIViewController {
    
    var newsImageUrl = ""
    var newsTitle = ""
    var newsContent = ""
    var newsAuhtor = ""
    var newsDate = ""
    
    let baseUrlImage = "\(Utils.base_url_image)storage/news/" 
    
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsPenulis: UILabel!
    @IBOutlet weak var newsIsi: UILabel!
    @IBOutlet weak var newsTanggal: UILabel!
    @IBOutlet weak var newsJudul: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title              = newsTitle
        setupDetail()
//        configView()
        
        
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                // User Interface is Dark
                
                newsJudul.textColor  = .black
                newsIsi.textColor    = .black
            } else {
                // User Interface is Light
                 newsJudul.textColor = .black
                newsIsi.textColor    = .black
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        EmporeHelper.setLightNav(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           EmporeHelper.setLightNav(self)
       }
    
    func setupDetail(){
        let url_image = URL(string: baseUrlImage + newsImageUrl)
        newsImage.sd_setImage(with: url_image, placeholderImage: UIImage.init(imageLiteralResourceName: "noimage"), options: [], completed: nil)
        
        newsJudul.text = newsTitle
        newsPenulis.text = "By \(newsAuhtor)"
        newsTanggal.text = newsDate
        newsIsi.setHTMLFromString(htmlText: newsContent)
    }
    
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Trait collection has already changed
        if #available(iOS 12.0, *) {
            let userInterfaceStyle = traitCollection.userInterfaceStyle
            
            if  userInterfaceStyle == .dark {
                // User Interface is Dark
                
                newsJudul.textColor  = .black
                newsIsi.textColor    = .black
            } else {
                // User Interface is Light
                newsJudul.textColor = .black
                newsIsi.textColor    = .black
            }

            
        } else {
            // Fallback on earlier versions
        }
    }
    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        
        hideKeyboardWhenTappedAround()
        
    }
    
    func setImageTitle(){
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "emhr_logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
