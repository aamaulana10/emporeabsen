//
//  InfoController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import XLPagerTabStrip

class InfoController: ButtonBarPagerTabStripViewController {
    
    var screenSize = UIScreen.main.bounds
    
    @IBOutlet weak var tabBarView: ButtonBarView!
    override func viewDidLoad() {
        
        EmporeHelper.initDefaultTab(self, fill:true)
        super.viewDidLoad()
//        configView()
        tabBarView.dropShadowBottom(radius: 2)
        
        EmporeHelper.setDarkNav(self)
    }
        
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .primary
        self.tabBarController?.tabBar.isHidden = false
        EmporeHelper.setDarkNav(self)
//        
        setImageTitle()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    // MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child1 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewsController") as! NewsController
        child1.tabTitle = "News"
        
        let child2 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MemoController") as! MemoController
        child2.tabTitle = "Memo"
        
        return [child1, child2]
    }

    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        
        hideKeyboardWhenTappedAround()
        
    }
    
    func setImageTitle(){
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "emhr_logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }

    
    
}
