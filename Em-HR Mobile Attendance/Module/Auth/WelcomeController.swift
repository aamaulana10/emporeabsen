//
//  ViewController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MaterialComponents

class WelcomeController: UIViewController {
    
    @IBOutlet weak var img_logo: UIImageView!
    @IBOutlet weak var img_logoHeight: NSLayoutConstraint!
    @IBOutlet weak var nik: MDCTextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var password: MDCTextField!
    @IBOutlet weak var inputContainer: UIView!
    @IBOutlet weak var inputContainerHeight: NSLayoutConstraint!
    
    var NikMcd  : MDCTextInputControllerOutlined?
    var passMcd : MDCTextInputControllerOutlined?
    
    var activityView: UIActivityIndicatorView?
    
    var progressView    = ProgressView()
    
    var code            = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configView()
        getCode()
        
        checkIsLogin()
        
        NikMcd      = FormHelper.textLayout(nik)
        passMcd     = FormHelper.textLayout(password)
        EmporeHelper.setPasswordEye(self, password, #selector(self.pwdListener(_:)))
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(back))
        self.navigationItem.leftBarButtonItem = newBackButton

    }
    
    @objc func back(sender: UIBarButtonItem) {
          
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"has_login")
        prefs.removeObject(forKey:"has_code")
        
        self.navigationController?.popViewController(animated: true)
      }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
//        setImageTitle()
                nik.text = ""
                password.text = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @objc func pwdListener(_ sender: UIButton){
           EmporeHelper.passwordEyeTextHide(sender, password)
       }
    
    func checkIsLogin(){
         let session = UserDefaults.standard.bool(forKey: "has_login")
                let infoScreenSession = UserDefaults.standard.bool(forKey: "info_screen")
                if session == true
                {
                     self.performSegue(withIdentifier: "gotoHome", sender: self)
                }
                else
                {
                    print("not login yet")
                }
    }
    
    func getCode(){
        if let companyCode = UserDefaults.standard.object(forKey: "code") as? String {
            code = companyCode
            
            print(code)
        }

    }
    
    
    @IBAction func backToCode(_ sender: UIButton) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"has_login")
        prefs.removeObject(forKey:"has_code")
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        login()
    }
    
    
    func login(){
        progressView.show(self)
        
        let base_Url = Utils.base_url+"/login?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json"
        ]
        
        let login_params : [String : Any] = [
            "nik" : nik.text ?? "",
            "password" : password.text ?? ""
        ]
        
        Alamofire.request(base_Url, method: .post, parameters: login_params, encoding: JSONEncoding.default, headers: headersku)
                   .responseJSON { response in
                       
                       if let error = response.result.error as NSError? {
                           print("error check code",error)
                           
                           if error.code == -1009 {
                               DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                                self.progressView.hide()
                                   self.login()
                                   
                               }
                           }
                       }else {
                        self.progressView.hide()
                           if response.result.value != nil {
                               switch response.result {
                                   
                               case .success:
                                   
                                   let responseStatus : JSON = JSON(response.result.value!)
                                   let status = responseStatus["status"]
                                   let token = responseStatus["data"]["user"]["apikey"]
                                   
                                   if(status == "error"){
                                       print("error ", response.result.value!)
                                    self.progressView.hide()
                                    let title = "Data not found"
                                    let message = "Please check your data & try again..."
                                    self.showAlert(title: title, message: message)
                                    
                                   }else {
                                    
                                    print("respon ",responseStatus)
                                       
                                    UserDefaults.standard.set(token.stringValue, forKey: "token")
                                    UserDefaults.standard.set(true, forKey: "has_login")
                                    self.performSegue(withIdentifier: "gotoHome", sender: self)

                                   }
                                   
                               case .failure(let error):
                                   print(error.localizedDescription)
                                   self.progressView.hide()
                                let title = "Service Unavailable"
                                let message = "Please try again letter..."
                                self.showAlert(title: title, message: message)
                               }
                               
                           }
                           
                       }
                       
               }
        
        
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }

    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        setInputContainer()
        hideKeyboardWhenTappedAround()
        makeBtnRounded()
        dynamicHeightImageLogo()
//        transparentTextField()
    }
    
    func setInputContainer(){
        inputContainer.layer.cornerRadius = 10
        inputContainer.backgroundColor = UIColor(white: 1, alpha: 0.5)
        inputContainer.layer.borderWidth = 4
        inputContainer.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
    }
    
    func dynamicHeightImageLogo(){
        let height = UIScreen.main.bounds.height
        
        img_logoHeight.constant = height / 15
    }
    
    func transparentTextField(){
        nik.backgroundColor = UIColor.clear
        password.backgroundColor = UIColor.clear
    }
    
    func makeBtnRounded(){
        btnLogin.layer.cornerRadius = 3
    }
        
    func setImageTitle(){
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))

        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "emhr_logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}

