//
//  CodeCompanyController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MaterialComponents

class CodeCompanyController: UIViewController {
    
    
    @IBOutlet weak var code: MDCTextField!
    @IBOutlet weak var inputContainer: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var activityView: UIActivityIndicatorView?
    
    var codeMcd : MDCTextInputControllerOutlined?
    var progressView    = ProgressView()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        configView()
        checkIsLogin()
        code.text = ""
        codeMcd = FormHelper.textLayout(code)
                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkIsLogin()
    }
    
    
    func checkIsLogin(){
        let has_login = UserDefaults.standard.bool(forKey: "has_login")
        let code = UserDefaults.standard.bool(forKey: "has_code")
        if code == true
        {
            
            if has_login == true {
                self.performSegue(withIdentifier: "has_Login", sender: self)
                
            }else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "Welcome") as! WelcomeController
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        else
        {
            print("started on code")
        }
    }
    
    @IBAction func Login(_ sender: UIButton) {
        checkCode()
    }
    
    func checkCode() {
        
        progressView.show(self)
        
        let companyID = code.text ?? ""
        
        let base_Url = Utils.base_url+"/check_code?company_code=" + companyID
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json"
        ]
        
        Alamofire.request(base_Url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error.code)
                    
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.checkCode()
                            
                        }
                    } else if(error.code == 0){
                        
                        self.progressView.hide()
                        let title = "Did you input correct code ?"
                        let message = "Wrong code, please check your code, maybe you have whitespace, please clear it & input again"
                        self.showAlert(title: title, message: message)
                    }
                }else {
                    
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            let responseStatus : JSON = JSON(response.result.value!)
                            let status = responseStatus["status"]
                            
                            if(status == "error"){
                                print("error ", response.result.value!)
                                self.progressView.hide()
                                let title = "Did you input correct code ?"
                                let message = "Wrong code, please check your code & input again"
                                self.showAlert(title: title, message: message)
                                
                            }else {
                                self.progressView.hide()
                                UserDefaults.standard.set(self.code.text!, forKey: "code")
                                UserDefaults.standard.set(true, forKey: "has_code")
                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "Welcome") as! WelcomeController
                                
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            self.progressView.hide()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
                
        }
        
    }
    
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        inputContainer.layer.cornerRadius = 10
        inputContainer.backgroundColor = UIColor(white: 1, alpha: 0.5)
        inputContainer.layer.borderWidth = 4
        inputContainer.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
        hideKeyboardWhenTappedAround()
        makeBtnRounded()
    }
    
    func makeBtnRounded(){
        btnSubmit.layer.cornerRadius = 3
    }
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
}
