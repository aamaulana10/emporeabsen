//
//  RegisterController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MaterialComponents

class RegisterController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var inputContainer: UIView!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var fullname: MDCTextField!
    @IBOutlet weak var position: MDCTextField!
    @IBOutlet weak var email: MDCTextField!
    @IBOutlet weak var company: MDCTextField!
    @IBOutlet weak var bussiness: MDCTextField!
    @IBOutlet weak var phone: MDCTextField!
    @IBOutlet weak var needInfo: UILabel!
    
    var activityView: UIActivityIndicatorView?
    
     var progressView    = ProgressView()
    
    
    var arrPosition = ["OWNER", "HRD / FINANCE", "IT", "OTHERS"]
    var arrBussiness = ["Agriculture / Mining","Business Service","Computers and Electronics","Consumer Service","Education","Energy & Utilities","Financial Service","Goverment","Healtcare, Pharmaceuticals, & Biotech","Manufacturing","Media & Entertaiment","Non Profit","Real Estate & Contruction","Retail","Software & Internet","Telecomunications","Transportation & Storage","Travel, Recreation, & Leisure","Wholesale & Distribution","Customer Products","Others"]
    
    var positionPicker = UIPickerView()
    var bussinessPicker = UIPickerView()
    
    var arrTf                   = [MDCTextField]()
    var arrTextLayout           = [MDCTextInputControllerOutlined]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configView()
        
        arrTf                   = [fullname,position,email,company,bussiness,phone]
        for tf in arrTf {
            let textLayout      = FormHelper.textLayout(tf)
            arrTextLayout.append(textLayout)
        }

        

        if #available(iOS 12.0, *) {
                   if self.traitCollection.userInterfaceStyle == .dark {
                       // User Interface is Dark
                       
                       needInfo.textColor  = .black
                   } else {
                       // User Interface is Light
                       needInfo.textColor = .black
                   }
               } else {
                   // Fallback on earlier versions
               }
        
        positionPicker.delegate = self
        bussinessPicker.delegate = self
        
        position.inputView = positionPicker
        bussiness.inputView = bussinessPicker
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        setImageTitle()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        //        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        //        navigationController?.navigationBar.shadowImage = nil
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
           // Trait collection has already changed
           if #available(iOS 12.0, *) {
               let userInterfaceStyle = traitCollection.userInterfaceStyle
               
               if  userInterfaceStyle == .dark {
                   // User Interface is Dark
                   
                   needInfo.textColor  = .white
               } else {
                   // User Interface is Light
                   needInfo.textColor = .black
               }

               
           } else {
               // Fallback on earlier versions
           }
       }
    
    
    @IBAction func register(_ sender: UIButton) {
        
        doRegister()
    }
    
    func doRegister(){
        if fullname.text == "" && position.text == "" && email.text == "" && company.text == "" && bussiness.text == "" && phone.text == "" {
            
            let title = "Incomplete Registration"
            let message = "Please check your data & try again..."
            self.showAlert(title: title, message: message)
        }else {
            
            progressView.show(self)
            
            let base_Url = Utils.base_url+"/register"
            let headersku: HTTPHeaders = [
                "Content-Type":"application/json"
            ]
            
            let login_params : [String : Any] = [
                "nama" : fullname.text ?? "",
                "jabatan" : position.text ?? "",
                "email" : email.text ?? "",
                "nama_perusahaan" : company.text ?? "",
                "bidang_usaha" : bussiness.text ?? "",
                "handphone" : phone.text ?? ""
            ]
            
            Alamofire.request(base_Url, method: .post, parameters: login_params, encoding: JSONEncoding.default, headers: headersku)
                .responseJSON { response in
                    
                    if let error = response.result.error as NSError? {
                        print("error check code",error)
                        
                        if error.code == -1009 {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                                self.progressView.hide()
                                self.doRegister()
                                
                            }
                        }
                    }else {
                        self.progressView.hide()
                        if response.result.value != nil {
                            switch response.result {
                                
                            case .success:
                                
                                let responseStatus : JSON = JSON(response.result.value!)
                                let status = responseStatus["status"]
                                
                                if(status == "error"){
                                    print("error ", response.result.value!)
                                    self.progressView.hide()
                                    let title = "Register failed"
                                    let message = "Please check your data & try again..."
                                    self.showAlert(title: title, message: message)
                                    
                                }else {
                                    
                                    print("respon ",responseStatus)
                                    
                                    
                                    let title = "Register success"
                                    let message = responseStatus["message"].stringValue
                                    self.showAlertSuccess(title: title, message: message)
                                }
                                
                            case .failure(let error):
                                print(error.localizedDescription)
                                self.progressView.hide()
                                let title = "Service Unavailable"
                                let message = "Please try again letter..."
                                self.showAlert(title: title, message: message)
                            }
                            
                        }
                        
                    }
                    
            }
        }
    }
    
    
    @IBAction func openCompanySite(_ sender: UIButton) {
        
        if let url = URL(string: "https://www.em-hr.co.id/") {
            UIApplication.shared.open(url)
        }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == positionPicker {
            return arrPosition.count
        }else {
            return arrBussiness.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == positionPicker {
            return arrPosition[row]
        }else {
            return arrBussiness[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == positionPicker {
            position.text = arrPosition[row]
            self.view.endEditing(false)
        }else {
            bussiness.text = arrBussiness[row]
            self.view.endEditing(false)
        }
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func showAlertSuccess(title: String, message: String){
           let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
           
           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
               self.fullname.text = ""
               self.position.text = ""
               self.email.text = ""
               self.company.text = ""
               self.bussiness.text = ""
               self.phone.text = ""
               self.navigationController?.popViewController(animated: true)
            
           }))
        
        self.present(alert, animated: true)
    }
    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        inputContainer.layer.cornerRadius = 10
        inputContainer.backgroundColor = UIColor(white: 1, alpha: 0.5)
        inputContainer.layer.borderWidth = 4
        inputContainer.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
        hideKeyboardWhenTappedAround()
        makeBtnRounded()
    }
    
    func makeBtnRounded(){
        btnRegister.layer.cornerRadius = 3
    }
    
    func setImageTitle(){
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "emhr_logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
}
