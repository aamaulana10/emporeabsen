//
//  RiwayatController.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RiwayatController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var riwayatTable: UITableView!
    
    var activityView: UIActivityIndicatorView?
    
    var cellName = "RiwayatCell"
    var screenSize = UIScreen.main.bounds
    
    var code = ""
    var token = ""
    
    var dataHistory : [History]?
    
    var refresher   = UIRefreshControl()
    
    var progressView = ProgressView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTableView()
        configView()
        getHistory()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        setImageTitle()
        
        configTableView()
        configView()
        getHistory()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func getCode(){
        if let companyCode = UserDefaults.standard.object(forKey: "code") as? String {
            code = companyCode
            
            print(code)
        }
        
    }
    
    func getToken(){
        if let dataToken = UserDefaults.standard.object(forKey: "token") as? String {
            token = dataToken
            
            print(token)
        }
        
    }
    
    func getHistory(){

        progressView.show(self, "Updating Data ...")
        
        getCode()
        getToken()
        
        let base_Url = Utils.base_url+"/history?company=" + code
        
        let headersku: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization" : "Bearer \(token)"
        ]
        
        Alamofire.request(base_Url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headersku)
            .responseJSON { response in
                
                if let error = response.result.error as NSError? {
                    print("error check code",error)
//                    self.hideActivityIndicator()
                    if error.code == -1009 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            
                            self.getHistory()
                            
                        }
                    }
                }else {
                      self.progressView.hide()
                    if response.result.value != nil {
                        switch response.result {
                            
                        case .success:
                            
                            let responseStatus : JSON = JSON(response.result.value!)
                            let status = responseStatus["status"]
                            let data = responseStatus["data"]
                            
                            if(status == "error"){
                                print("error ", response.result.value!)
                                let title = "Unstable Connection"
                                let message = "Please check your Connection & try again..."
                                self.showAlert(title: title, message: message)
                                
                            }else {
                                
                                print("respon history ",data)
                                
                                do{
                                    
                                    self.dataHistory = try JSONDecoder().decode([History].self, from: data.rawData())
                                    
                                    self.riwayatTable.reloadData()
        
                                }
                                catch{
                                    print("error catch ",error)
                                }
                            }
                            
                        case .failure(let error):
                            print(error)
                             self.progressView.hide()
                            let title = "Service Unavailable"
                            let message = "Please try again letter..."
                            self.showAlert(title: title, message: message)
                        }
                        
                    }
                    
                }
        }
        
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    
    
    func configTableView(){
        riwayatTable.delegate = self
        riwayatTable.dataSource = self
        
        riwayatTable.register(UINib.init(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        
        //riwayatTable.rowHeight = screenSize.height / 8
        riwayatTable.backgroundColor = .clear
        riwayatTable.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataHistory?.count ?? 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RiwayatCell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath) as! RiwayatCell
        
        let data = dataHistory?[indexPath.row]
        
        let attendanceIn = dataHistory?[indexPath.row].attendance_type_in
        let attendanceOut = dataHistory?[indexPath.row].attendance_type_out

        if(data != nil){
            cell.hideGradient()
            cell.time.text = "\(EmporeHelper.convertDateWithDayFormater(data?.date ?? "2020-00-00")) \(data?.timezone ?? "")"
            cell.clockIn.text = "\(data?.clock_in ?? "") \(attendanceIn == "normal" ? "" : attendanceIn == "remote" ? "(R)" : "(O)")"
            cell.clockOut.text = "\(data?.clock_out ?? "") \(attendanceOut == "normal" ? "" : attendanceIn == "remote" ? "(R)" : "(O)")"
            cell.duration.text = data?.work_time
        }
        
        if(data?.late == nil){

            cell.clockIn.textColor  = .primary
        }else {
            
            cell.clockIn.textColor  = .red
        }
        
        if(data?.early == nil){
            
            cell.clockOut.textColor = .primary
        }else {
            
            cell.clockOut.textColor = .red
            
        }
        
        
        cell.container.layer.cornerRadius = 5
        cell.container.backgroundColor = UIColor(white: 1, alpha: 0.5)
        cell.container.layer.borderWidth = 4
        cell.container.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
        cell.containerHeight.constant = screenSize.height / 6
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        return cell
    }
    
    func configView(){
        setGradientBackground(colorTop: UIColor.init(hexString: "#0E9A88"), colormid: UIColor.init(hexString: "#AED581"), colorBottom: UIColor.init(hexString: "#FAFAFA"))
        
        hideKeyboardWhenTappedAround()
        
    }
    
    func setImageTitle(){
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "emhr_logo")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    func setGradientBackground(colorTop: UIColor,colormid: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colormid.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        //        gradientLayer.locations = [1, 0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
