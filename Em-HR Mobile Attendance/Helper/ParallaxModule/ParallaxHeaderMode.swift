//
//  ParallaxHeaderMode.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 27/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation

public enum ParallaxHeaderMode: Int {
    
    case fill = 0
    
    case top
   
    case topFill
   
    case center
    
    case centerFill
    
    case bottom
    
    case bottomFill
}
