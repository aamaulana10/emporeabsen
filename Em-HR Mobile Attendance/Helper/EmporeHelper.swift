//
//  EmporeHelper.swift
//  Em-HR Mobile Attendance
//
//  Created by aamaulana10 on 26/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation
import UIKit
import XLPagerTabStrip

class EmporeHelper {
    
    static func setPasswordEye(_ sender: UIViewController,_ textField:UITextField, _ selector:Selector){
        let rightButton                 = UIButton(type: .custom)
        rightButton.setImage(UIImage(named: "eyeoff"), for: .normal)
        rightButton.frame               = CGRect(x:0, y:0, width: 24, height: 24)
        rightButton.applyViewNavBarConstraints(width: Float(24), height: Float(24))
        rightButton.addTarget(sender, action: selector, for: .touchUpInside)
        textField.rightViewMode         = .always
        textField.rightView             = rightButton
    }
    
    static func passwordEyeTextHide (_ btn: UIButton, _ textField:UITextField){
       if (btn.tag == 0){
           textField.isSecureTextEntry  = false
           btn.tag                      = 1
           btn.setImage(UIImage(named: "eye"), for: .normal)
       }else{
           textField.isSecureTextEntry  = true
           btn.tag                      = 0
           btn.setImage(UIImage(named: "eyeoff"), for: .normal)
       }
    }
    
    static func initDefaultTab(_ con:UIViewController, fill:Bool){
      if let sender = con as? ButtonBarPagerTabStripViewController {
        sender.settings.style.buttonBarBackgroundColor                  = .primary
        sender.settings.style.buttonBarItemBackgroundColor              = .primary
          sender.settings.style.selectedBarBackgroundColor                = .white
          sender.settings.style.buttonBarItemFont                         = .boldSystemFont(ofSize: 14)
          sender.settings.style.selectedBarHeight                         = 2.0
          sender.settings.style.buttonBarMinimumLineSpacing               = 48
          sender.settings.style.buttonBarItemTitleColor                   = .white
          sender.settings.style.buttonBarItemsShouldFillAvailableWidth    = fill
          sender.settings.style.buttonBarLeftContentInset                 = 0
          sender.settings.style.buttonBarRightContentInset                = 0
          
          sender.changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
              guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
              newCell?.label.textColor = .white
          }
        }else{
            print("View Controller must be ButtonBarPagerTabStripViewController")
        }
    }
    
    static func instantiateVC(_ storyboard: UIStoryboard, _ id: String) -> UIViewController? {
           if #available(iOS 13.0, *) {
               return storyboard.instantiateViewController(identifier: id)
           } else {
               return storyboard.instantiateViewController(withIdentifier: id)
           }
       }
    
    static func pushWithHideBottomBar(_ sender:UIViewController, _ dest:UIViewController){
           sender.tabBarController?.tabBar.isHidden    = true
           sender.navigationController?.pushViewController(dest, animated: true)
       }
        
    // ======================= NAVIGATIONS ================== //
    
    @available(iOS 13.0, *)
    static func navAppearance (_ bgColor:UIColor, _ textAttr:[NSAttributedString.Key : UIColor]) -> UINavigationBarAppearance {
        let backAppearance                          = UIBarButtonItemAppearance()
        backAppearance.normal.titleTextAttributes   = [NSAttributedString.Key.foregroundColor: UIColor.clear]
        let appearance                              = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor                  = bgColor
        appearance.titleTextAttributes              = textAttr
        appearance.shadowColor                      = .clear
        appearance.backButtonAppearance             = backAppearance
        return appearance
    }
    
    static func setDarkNav(_ con:UIViewController){
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        if #available(iOS 13.0, *) {
            let appearance                                                  = navAppearance(.primary, textAttributes)
            con.navigationController?.navigationBar.standardAppearance      = appearance
            con.navigationController?.navigationBar.scrollEdgeAppearance    = appearance
            con.navigationController?.navigationBar.compactAppearance       = appearance
            
            //            if let nav = con.navigationController as? NavbarController {
            //               nav.requiredStatusBarStyle = .darkContent
            //            }
        }
        con.navigationController?.navigationBar.barStyle                    = .black
        con.navigationController?.navigationBar.barTintColor                = .primary
        con.navigationController?.navigationBar.tintColor                   = .white
        con.navigationController?.navigationBar.isTranslucent               = false
        con.navigationController?.navigationBar.titleTextAttributes         = textAttributes
        con.navigationController?.navigationBar.shadowImage                 = nil
        con.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        con.navigationController?.navigationBar.shadowImage                 = UIImage()
    }
    
    static func setLightNav(_ con:UIViewController){
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkText]
        if #available(iOS 13.0, *) {
            let appearance                                                  = navAppearance(.white, textAttributes)
            con.navigationController?.navigationBar.standardAppearance      = appearance
            con.navigationController?.navigationBar.scrollEdgeAppearance    = appearance
            con.navigationController?.navigationBar.compactAppearance       = appearance
            
        }
        con.navigationController?.navigationBar.barStyle                    = .default
        con.navigationController?.navigationBar.barTintColor                = .white
        con.navigationController?.navigationBar.tintColor                   = .darkText
        con.navigationController?.navigationBar.isTranslucent               = true
        con.navigationController?.navigationBar.titleTextAttributes         = textAttributes
        con.navigationController?.navigationBar.shadowImage                 = nil
        con.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        con.navigationController?.navigationBar.shadowImage                 = UIImage()
        setTransparentStatusBar(con)
    }
    
    static func setTransparentStatusBar(_ sender: UIViewController){
        sender.setNeedsStatusBarAppearanceUpdate()
        let window          = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        for subViews in window!.subviews {
            if(subViews.tag == 919){
                subViews.removeFromSuperview()
            }
        }
        var statusBarView:UIView
        if #available(iOS 13.0, *) {
            statusBarView   = UIView(frame: (window?.windowScene?.statusBarManager!.statusBarFrame)!)
        } else {
            statusBarView   = UIApplication.shared.value(forKey: "statusBar") as! UIView
        }
        statusBarView.backgroundColor   = .primary
        statusBarView.tag               = 919
        window!.addSubview(statusBarView)
    }
    
    public static func addBackButton(_ con: UIViewController) -> UIBarButtonItem {
        let btnBack = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        btnBack.applyViewNavBarConstraints(width: 22, height: 22)
      
        let itemBack = UIBarButtonItem(customView: btnBack)
        
        return itemBack
    }
    
    static func addMultiRightBarButtons( _ con :UIViewController, _ selectors: [Selector], _ images:[UIImage]){
        DispatchQueue.main.async {
            var i = 0
            var barButtonItems = [UIBarButtonItem]()
            selectors.forEach({ (sel) in
                let btn    = UIButton(type: .custom)
                btn.setImage(images[i], for: .normal)
                btn.frame  = CGRect(x: 0, y: 0, width: 24, height: 24)
                btn.applyViewNavBarConstraints(width: Float(24), height: Float(24))
                btn.addTarget(con, action: sel, for: .touchUpInside)
                let item   = UIBarButtonItem(customView: btn)
                barButtonItems.append(item)
                i += 1
            })
            con.navigationItem.setRightBarButtonItems(barButtonItems, animated: true)
        }
    }
    
    static func removeRightTextBarButton( _ con :UIViewController){
        DispatchQueue.main.async {
            con.navigationItem.setRightBarButtonItems([], animated: true)
        }
    }
    
    static func addRightTextBarButton( _ con :UIViewController, _ selector: Selector, _ title:String){
        DispatchQueue.main.async {
            let item1           = UIBarButtonItem(title: title, style: .done, target: con, action: selector)
            if((con.navigationController?.navigationBar.barStyle)! == UIBarStyle.black){
                item1.tintColor     = .white
            }else{
                item1.tintColor     = .primary
            }
            
            con.navigationItem.setRightBarButtonItems([item1], animated: true)
        }
    }
    
    static func addRightBarButton( _ con :UIViewController, _ selector: Selector, _ image:UIImage){
        DispatchQueue.main.async {
            let btn1    = UIButton(type: .custom)
            btn1.setImage(image, for: .normal)
            btn1.frame  = CGRect(x: 0, y: 0, width: 24, height: 24)
            btn1.applyViewNavBarConstraints(width: Float(24), height: Float(24))
            btn1.addTarget(con, action: selector, for: .touchUpInside)
            let item1   = UIBarButtonItem(customView: btn1)
            con.navigationItem.setRightBarButtonItems([item1], animated: true)
        }
    }
    
    static func addLeftBarButton( _ con :UIViewController, _ selector: Selector, _ image:UIImage){
        DispatchQueue.main.async {
            let btn1    = UIButton(type: .custom)
            btn1.setImage(image, for: .normal)
            btn1.frame  = CGRect(x: 0, y: 0, width: 24, height: 24)
            btn1.applyViewNavBarConstraints(width: Float(24), height: Float(24))
            btn1.addTarget(con, action: selector, for: .touchUpInside)
            let item1   = UIBarButtonItem(customView: btn1)
            con.navigationItem.setLeftBarButtonItems([item1], animated: true)
        }
    }
    
    static func convertDateFormater(_ date: String) -> String
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           let date = dateFormatter.date(from: date)
           dateFormatter.dateFormat = "EEEE, d MMMM yyyy"
           return  dateFormatter.string(from: date!)

       }
    
    static func convertDateWithDayFormater(_ date: String) -> String
         {
             let dateFormatter = DateFormatter()
             dateFormatter.dateFormat = "yyyy-MM-dd"
             let date = dateFormatter.date(from: date)
             dateFormatter.dateFormat = "EEEE, d MMMM yyyy"
             return  dateFormatter.string(from: date!)

         }
    
    static func convertTimeFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "HH:mm:ss"
        return  dateFormatter.string(from: date!)

    }
    
    static func convertStringToDateFormater(_ date: String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: date)!

    }
    
    static func convertStringToHourFormater(_ date: String) -> Date
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "HH:mm"
           return dateFormatter.date(from: date)!

       }
        
}
