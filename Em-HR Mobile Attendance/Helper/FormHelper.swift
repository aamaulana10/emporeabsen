//
//  FormHelper.swift
//  Em-HR Mobile Attendance
//
//  Created by aamaulana10 on 26/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents

class FormHelper {
    
    static func textLayout(_ textField: MDCTextField) -> MDCTextInputControllerOutlined {
         let textLayout = MDCTextInputControllerOutlined(textInput: textField)
         textLayout.activeColor                      = .primary
         textLayout.floatingPlaceholderActiveColor   = .primary
         textField.clearButtonMode                   = .never
         return textLayout
     }
     static func textLayout(_ textField: MDCMultilineTextField) -> MDCTextInputControllerOutlinedTextArea {
         let textLayout = MDCTextInputControllerOutlinedTextArea(textInput: textField)
         textLayout.activeColor                      = .primary
         textLayout.floatingPlaceholderActiveColor   = .primary
         textField.clearButtonMode                   = .never
         return textLayout
     }
}
