//
//  StoryboardExtension.swift
//  Em-HR Mobile Attendance
//
//  Created by aamaulana10 on 02/05/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static let mainStoryboard       = UIStoryboard(name: "Main", bundle: nil)
}
