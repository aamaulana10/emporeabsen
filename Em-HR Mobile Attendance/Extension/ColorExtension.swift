//
//  ColorExtension.swift
//  Em-HR Mobile Attendance
//
//  Created by aamaulana10 on 26/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static let primary              = UIColor(hexString: "0E9A88")
}
