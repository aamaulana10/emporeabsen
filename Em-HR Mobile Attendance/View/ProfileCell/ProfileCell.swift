//
//  ProfileCell.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 28/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {


    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var CONTAINER: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
