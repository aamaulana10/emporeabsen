//
//  ShiftCell.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 16/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit

class ShiftCell: UITableViewCell {

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var clockIn: UILabel!
    @IBOutlet weak var clockOut: UILabel!
    @IBOutlet weak var duration: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
