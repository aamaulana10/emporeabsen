//
//  RiwayatCell.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 23/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit
import SkeletonView

class RiwayatCell: UITableViewCell {

    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var clockIn: UILabel!
    @IBOutlet weak var clockOut: UILabel!
    @IBOutlet weak var duration: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        showGradient()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8))
    }
    
    func showGradient(){
        time.showAnimatedGradientSkeleton()
        clockOut.showAnimatedGradientSkeleton()
        clockIn.showAnimatedGradientSkeleton()
        duration.showAnimatedGradientSkeleton()
    }
    
    func hideGradient(){
        clockOut.hideSkeleton()
        clockIn.hideSkeleton()
        time.hideSkeleton()
        duration.hideSkeleton()
    }
    
}
