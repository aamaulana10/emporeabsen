//
//  NewsCell.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 23/03/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var newsThumbnail: UIImageView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var Container: UIView!
    @IBOutlet weak var ContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var titleCell: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
