//
//  NewsHeaderCell.swift
//  Em-HR Mobile Attendance
//
//  Created by aa maulana10 on 11/04/20.
//  Copyright © 2020 Empore. All rights reserved.
//

import UIKit

class NewsHeaderCell: UITableViewCell {

    @IBOutlet weak var headerSubTitle: UILabel!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var textHeader: UILabel!
    @IBOutlet weak var container: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
